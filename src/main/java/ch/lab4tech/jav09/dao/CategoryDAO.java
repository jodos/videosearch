package ch.lab4tech.jav09.dao;

import java.util.List;

import ch.lab4tech.jav09.model.Category;

public interface CategoryDAO {
	public List<Category> find();
}
